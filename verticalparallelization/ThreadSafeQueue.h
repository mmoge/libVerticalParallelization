///
/// \file   ThreadSafeQueue.h
/// \brief  Simple implementation of a thread safe queue (FIFO) using std::mutex and std::condition_variable
/// \author Maxime Mogé
/// \date   2018
///
///
///
#ifndef HELPER_THREAD_SAFE_QUEUE_H
#define HELPER_THREAD_SAFE_QUEUE_H

#include <mutex>
#include <condition_variable>

namespace verticalparallelization
{

/// \brief Simple implementation of a thread safe queue (FIFO) using std::mutex and std::condition_variable
/// Templated on the type of data in the queue
template<typename T>
class ThreadSafeQueue
{

private:

    struct node
    {
        T data;
        node* next;
    };
    node* head;
    node* tail;
    std::mutex mut;
    std::condition_variable cond;
    bool stopThreads;

public:

    ThreadSafeQueue()
      : head(NULL)
      , tail(NULL)
      , stopThreads(false)
    {

    }

    ~ThreadSafeQueue()
    {
    }

    ThreadSafeQueue(const ThreadSafeQueue& other)=delete;
    ThreadSafeQueue& operator=(const ThreadSafeQueue& other)=delete;

    void push(T& new_value)
    {
        std::unique_lock<std::mutex> lock(mut);
        node* p = new node;
        p->data = new_value;
        p->next = NULL;
        if ( !empty() ) {
            tail->next = p;
//            tail->next=std::move(p);
        }
        else {
            head = p;
//            head=std::move(p);
        }
        tail = p;
//        tail=head;
        cond.notify_one();
    }

    bool pop(T& value)
    {
      std::unique_lock<std::mutex> lock(mut);
      while( empty() && !stopThreads )
      {
          cond.wait(lock);
      }
      if (!stopThreads)
      {
          value = head->data;
          node* tmp = head->next;
          delete head;
          head = tmp;
          if( head == NULL ) {
              tail = NULL;
          }
          else {
          }
          return true;
      }
      else  //no task popped, terminate worker_thread
      {
          return false;
      }
    }

    ///WARNING: should be called only when mut is locked
    bool empty()
    {
        return (tail == NULL);
    }

    void stopAllThreads()
    {
        std::unique_lock<std::mutex> lock(mut);
        stopThreads = true;
        cond.notify_all();
    }
};


} // namespace verticalparallelization

#endif  //HELPER_THREAD_SAFE_QUEUE_H
