///
/// \file   memory_helper_functions.h
/// \brief  Helper functions to register all memory accesses done using the overloaded version of operator[] during the logging phase.
/// \author Maxime Mogé
/// \date   2018
///
///
///
#ifndef HELPER_MEMORY_HELPER_FUNCTION_H
#define HELPER_MEMORY_HELPER_FUNCTION_H

#include "memory_var.h"
#include "ParallelPhase.h"
#include "ParallelTask.h"
#include "ParallelSection.h"
#include <assert.h>
#include <future>

namespace verticalparallelization
{

/// \brief stores a pointer to current parallel section in a global variable
struct currentSection
{
    static BaseParallelSection* m_currentParallelSection;
};

//    thread_local __attribute__ ((tls_model ("initial-exec"))) ParallelTask* restrict currentTask;
/// Declaring the thread_local variable currentTask as static inside a function
/// with __attribute__ ((tls_model ("initial-exec")))
/// allows us to get better performances than using a straightforward/naive TLS variable.
/// A naive TLS variable induces a call to a function each time the variable is used.
inline ParallelTask*& getCurrentTask()
{
    static thread_local ParallelTask* restrict  __attribute__ ((tls_model ("initial-exec")))  currentTask  = nullptr;
    return currentTask;
}
void beginOrEndPhase( size_t* curCounter, ParallelTask*& restrict curTask );
void loggingFirstPhaseRead();
void loggingFirstPhaseWrite(size_t n, uintptr_t thisPtr);
void loggingSecondPhaseRead(size_t n, uintptr_t thisPtr);
void loggingSecondPhaseWrite(size_t n, uintptr_t thisPtr);
void loggingPhaseRead(size_t n, uintptr_t thisPtr);
void loggingPhaseWrite(size_t n, uintptr_t thisPtr);

/// \brief Called within the body of the overloaded version of operator[] - Registers the Read memory access represented by n and thisPtr for later dependency analysis
inline void subscriptOperatorOverloadRead( size_t n, uintptr_t thisPtr ) __attribute__((__always_inline__));
inline void subscriptOperatorOverloadRead( size_t n, uintptr_t thisPtr )
{
    // The use of __builtin_expect can yield more efficient branch prediction
    // as we assume that the most frequent case is the parallel case
    if( __builtin_expect (simuInfo::execType == ExecType::PARALLEL,1))
    {
        ParallelTask* restrict  curTask = getCurrentTask();
        size_t* restrict  curCounter = &curTask->m_currentPhaseAccessCounter;
        const size_t curLastAccess = curTask->m_currentPhaseLastAccess;
        // The use of __builtin_expect can yield more efficient branch prediction
        // as we assume that the least frequent case is when current access is the
        // first or the last of current Phase
        if ( __builtin_expect (*curCounter < curLastAccess,1) )
        {
            (*curCounter)++;
        }
        else
        {
            beginOrEndPhase(curCounter, curTask);
        }
    }
    else if(simuInfo::execType == ExecType::LOGGING)
    {
        loggingPhaseRead(n,thisPtr);
    }
}

/// \brief Called within the body of the overloaded version of operator[] - Registers the Write memory access represented by n and thisPtr for later dependency analysis
inline void subscriptOperatorOverloadWrite( size_t n, uintptr_t thisPtr ) __attribute__((__always_inline__));
inline void subscriptOperatorOverloadWrite( size_t n, uintptr_t thisPtr )
{
    // The use of __builtin_expect can yield more efficient branch prediction
    // as we assume that the most frequent case is the parallel case
    if( __builtin_expect (simuInfo::execType == ExecType::PARALLEL,1))
    {
        ParallelTask* restrict  curTask = getCurrentTask();
        size_t* restrict  curCounter = &curTask->m_currentPhaseAccessCounter;
                    const size_t curLastAccess = curTask->m_currentPhaseLastAccess;
        // The use of __builtin_expect can yield more efficient branch prediction
        // as we assume that the least frequent case is when current access is the
        // first or the last of current Phase
        if ( __builtin_expect (*curCounter < curLastAccess,1) )
        {
            (*curCounter)++;
        }
        else
        {
            beginOrEndPhase(curCounter, curTask);
        }
    }
    else if(simuInfo::execType == ExecType::LOGGING)
    {
        loggingPhaseWrite(n,thisPtr);
    }
}

///// \brief creates the eilck_obj if necessary
//eilck_obj* getEilckObj ( uintptr_t vecId, size_t blocId );

/// \brief used for debugging purposes - not sure if still valid
void printAllFIFOs();
/// \brief used for debugging purposes - not sure if still valid
void printFIFO( eilck_obj* obj);

}   //namespace verticalparallelization

#endif  //HELPER_MEMORY_HELPER_FUNCTION_H
