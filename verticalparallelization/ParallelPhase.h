///
/// \file   ParallelPhase.h
/// \brief  Implementation of the basic components of our parallelization : phase (called meta steps in the paper)
/// \author Maxime Mogé
/// \date   2018
///
///
///
#ifndef HELPER_PARALLEL_PHASE_H
#define HELPER_PARALLEL_PHASE_H

#include <vector>
#include <map>
#include <eilck++.hpp>
#include <eilck.h>
#include "memory_var.h"
#include "thread_info.h"

#ifdef PRINT_FIFOS
#include <mutex>
#endif //PRINT_FIFOS

namespace verticalparallelization
{

typedef unsigned int TaskId;
typedef unsigned int PhaseId;

/// \brief Implementation of the basic components of our parallelization : phase (called meta steps in the paper)
class ParallelPhase
{
public:
    /// id of the phase
    PhaseId m_phaseId;
    /// id of attached task in enclosing parallel section
    unsigned int m_taskId;
    /// index in attached Task of the last access of this Phase
    size_t m_lastAccess;
    /// lock object used to manage dependencies with Eilck
    eilck_obj* m_fifo;

    /// Maps the eilck_obj accessed in Read mode by current phase with an inclusive Handle - by We use 2 handles for each object so we can do a require before a release (to add the handle at the back of the FIFO)
    /// Quote from eilck documentation: Iterative lock access is easily modeled by regulating the access through two handles that alternate their roles. If the second handle requires insertion in the FIFO just before the first releases the lock, the access is cyclic in a guaranteed order. All is put in place such that such a pair can simply be declared as eilck∷ehdl[2] or eilck∷ihdl[2].
    std::map< eilck_obj*, eilck_ihdl*[2] > m_rAccesses;
    /// Maps the eilck_obj accessed in Write mode by current phase with an exclusive Handle - We use 2 handles for each object so we can do a require before a release (to add the handle at the back of the FIFO)
    /// Quote from eilck documentation: Iterative lock access is easily modeled by regulating the access through two handles that alternate their roles. If the second handle requires insertion in the FIFO just before the first releases the lock, the access is cyclic in a guaranteed order. All is put in place such that such a pair can simply be declared as eilck∷ehdl[2] or eilck∷ihdl[2].
    std::map< eilck_obj*, eilck_ehdl*[2] > m_wAccesses;
    /// for each eilck_obj*, contains 0 or 1 to indicate if the next handle on this object in the FIFO in the first or the second one in the pair
    mutable std::map< eilck_obj*, size_t > m_handleIterator;

public:
    ParallelPhase( PhaseId pid, unsigned int tid )
    : m_phaseId(pid),
      m_taskId(tid),
      m_lastAccess(0),
      m_fifo(),
      m_rAccesses(),
      m_wAccesses(),
      m_handleIterator()
    {
        assert( threadInfo::m_workerThreadId == 1 );
        this->m_fifo = new eilck_obj();
        eilck_obj_fixed_init( this->m_fifo, eilck_len );
#ifdef PRINT_FIFOS
        addressProfiling::m_objTracker[ this->m_fifo ] = this;
#endif //PRINT_FIFOS
        this->m_rAccesses.clear();
        this->m_wAccesses.clear();
        PRINT_MSG_PARALLEL(" creating m_rAccesses - size " << this->m_rAccesses.size());
        PRINT_MSG_PARALLEL(" creating m_wAccesses - size " << this->m_wAccesses.size());
        PRINT_LOG_STREAM_TO_FILE();
    }
    ~ParallelPhase()
    {
        for( std::map< eilck_obj*, eilck_ihdl*[2] >::iterator it = this->m_rAccesses.begin()
                ;it != m_rAccesses.end()
                ; ++it )
        {
            delete it->second[0];
            delete it->second[1];
        }
        for( std::map< eilck_obj*, eilck_ehdl*[2] >::iterator it = this->m_wAccesses.begin()
                ;it != this->m_wAccesses.end()
                ; ++it )
        {
            delete it->second[0];
            delete it->second[1];
        }
        this->m_rAccesses.clear();
        this->m_wAccesses.clear();
        this->m_handleIterator.clear();
    }
    PhaseId getId() const
    {
        return this->m_phaseId;
    }

    /// \brief Register a Read access from current phase to data owned by ownerPhase.
    /// If there is already a Write or a Read access from current phase to data owned by ownerPhase,
    /// do nothing, otherwise create a new ihdl.
    void addRAccess( ParallelPhase* ownerPhase )
    {
        eilck_obj* vecBloc = ownerPhase->m_fifo;
        assert(threadInfo::m_workerThreadId == 1 );
        //if this bloc already has a read or write handle in this phase, we do nothing
        if ( this->m_rAccesses.find(vecBloc) == this->m_rAccesses.end()
                && this->m_wAccesses.find(vecBloc) == this->m_wAccesses.end() )
        {
            this->m_rAccesses[vecBloc][0] = new eilck_ihdl();
            this->m_rAccesses[vecBloc][1] = new eilck_ihdl();
            this->m_handleIterator[vecBloc] = 0;
        }
    }

    /// \brief Register a Write access from current phase to data owned by ownerPhase.
    /// If there is already a Write from current phase to data owned by ownerPhase, do nothing.
    /// Else if there is already a Read from current phase to data owned by ownerPhase, we delete the corresponding ihdl.
    /// Else we just create a ehdl.
    void addWAccess( ParallelPhase* ownerPhase )
    {
        eilck_obj* vecBloc = ownerPhase->m_fifo;
        assert( threadInfo::m_workerThreadId == 1 );
        //if this bloc already has a read handle in this phase, we delete it
        if ( this->m_rAccesses.find(vecBloc) != this->m_rAccesses.end() )
        {
            this->m_rAccesses.erase( this->m_rAccesses.find(vecBloc) );
        }
        //add a write handle if there is not already one on vecBloc
        if ( this->m_wAccesses.find(vecBloc) == this->m_wAccesses.end() )
        {
            //create write Handle
            this->m_wAccesses[vecBloc][0] = new eilck_ehdl();
            this->m_wAccesses[vecBloc][1] = new eilck_ehdl();
            this->m_handleIterator[vecBloc] = 0;
        }
    }

    /// \brief requires all Handles in m_wAccesses and m_rAccesses.
    /// Require appends a handle to the tail of the specified FIFO.
    void requireHandles() const
    {
        for( std::map< eilck_obj*, size_t >::iterator it = this->m_handleIterator.begin();
                it != this->m_handleIterator.end();
                ++it )
        {
            it->second = ( it->second+1 ) %2;
        }
    #ifdef PRINT_FIFOS
        std::unique_lock<std::mutex> lock1(*addressProfiling::m_fifoTrackerMutex);
        for( std::map< eilck_obj*, eilck_ihdl*[2] >::iterator it = this->m_rAccesses.begin();
                it != this->m_rAccesses.end();
                ++it )
        {
            ParallelPhase* ownerPhase = addressProfiling::m_objTracker[it->first];
            //
            if(addressProfiling::m_fifoTracker.find(this->m_sectionId) == addressProfiling::m_fifoTracker.end())
            {
                //this is the first mem access we add to this section in m_fifoTracker
                addressProfiling::m_fifoTracker[this->m_sectionId] =
                    std::map< ParallelPhase*, std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>> >();
            }
            if(addressProfiling::m_fifoTracker[this->m_sectionId].find(ownerPhase) == addressProfiling::m_fifoTracker[this->m_sectionId].end())
            {
                //this is the first mem access we add to this eilck_obj in m_fifoTracker
                addressProfiling::m_fifoTracker[this->m_sectionId][ownerPhase] =
                        std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>>();
            }
            //
            PRINT_MSG_PARALLEL("m_fifoTracker requiring R handle " << it->second[m_handleIterator[it->first]] << " on obj " << it->first);
            PRINT_LOG_STREAM_TO_FILE();
            addressProfiling::m_fifoTracker[this->m_sectionId][ownerPhase].push_back(
                    std::pair<std::array<unsigned int,3>,std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>(
                            {1,this->m_taskId,this->m_phaseId},std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>(it->second[m_handleIterator[it->first]], nullptr)
                            ) );
        }
        for( std::map< eilck_obj*, eilck_ehdl*[2] >::iterator it = this->m_wAccesses.begin();
                it != this->m_wAccesses.end();
                ++it )
        {
            ParallelPhase* ownerPhase = addressProfiling::m_objTracker[it->first];
            //
            if(addressProfiling::m_fifoTracker.find(this->m_sectionId) == addressProfiling::m_fifoTracker.end())
            {
                //this is the first mem access we add to this section in m_fifoTracker
                addressProfiling::m_fifoTracker[this->m_sectionId] =
                    std::map< ParallelPhase*, std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>> >();
            }
            if(addressProfiling::m_fifoTracker[this->m_sectionId].find(ownerPhase) == addressProfiling::m_fifoTracker[this->m_sectionId].end())
            {
                //this is the first mem access we add to this eilck_obj in m_fifoTracker
                addressProfiling::m_fifoTracker[this->m_sectionId][ownerPhase] =
                        std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>>();
            }
            //
            PRINT_MSG_PARALLEL("m_fifoTracker requiring W handle " << it->second[m_handleIterator[it->first]] << " on obj " << it->first);
            PRINT_LOG_STREAM_TO_FILE();
            addressProfiling::m_fifoTracker[this->m_sectionId][ownerPhase].push_back(
                    std::pair<std::array<unsigned int,3>,std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>(
                            {0,this->m_taskId,this->m_phaseId},std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>(nullptr,it->second[m_handleIterator[it->first]])
                            ) );
        }
        lock1.unlock();
    #endif // PRINT_FIFOS
        for( std::map< eilck_obj*, eilck_ihdl*[2] >::const_iterator it = this->m_rAccesses.begin();
                it != this->m_rAccesses.end();
                ++it )
        {
            PRINT_MSG_PARALLEL("requiring R handle " << it->second[m_handleIterator[it->first]] << " on obj " << it->first);
            PRINT_LOG_STREAM_TO_FILE();
            eilck_ihdl_req( it->second[m_handleIterator[it->first]], it->first );
        }
        for( std::map< eilck_obj*, eilck_ehdl*[2] >::const_iterator it = this->m_wAccesses.begin();
                it != this->m_wAccesses.end();
                ++it )
        {
            PRINT_MSG_PARALLEL("requiring W handle " << it->second[m_handleIterator[it->first]] << " on obj " << it->first);
            PRINT_LOG_STREAM_TO_FILE();
            eilck_ehdl_req( it->second[m_handleIterator[it->first]], it->first );
        }
    }

    /// \brief releases all Handles in m_wAccesses and m_rAccesses.
    /// Release removes the handle from the head of the FIFO.
    void releaseHandles() const
    {
    #ifdef PRINT_FIFOS
        std::unique_lock<std::mutex> lock1(*addressProfiling::m_fifoTrackerMutex);
        for( std::map< eilck_obj*, eilck_ihdl*[2] >::iterator it = this->m_rAccesses.begin();
                it != this->m_rAccesses.end();
                ++it )
        {
            ParallelPhase* ownerPhase = addressProfiling::m_objTracker[it->first];
            PRINT_MSG_PARALLEL("m_fifoTracker releasing R handle " << it->second[(m_handleIterator[it->first]+1)%2] << " on obj " << it->first);
            PRINT_LOG_STREAM_TO_FILE();
            size_t tmpsize = addressProfiling::m_fifoTracker[this->m_sectionId][ownerPhase].size();
            std::list<std::pair<std::array<unsigned int,3>,std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>>::iterator it2 =
                    addressProfiling::m_fifoTracker[this->m_sectionId][ownerPhase].begin();
            while (it2 != addressProfiling::m_fifoTracker[this->m_sectionId][ownerPhase].end())
            {
                if ( std::get<0>(it2->first) == 1
                        && std::get<1>(it2->first) == this->m_taskId
                        && std::get<2>(it2->first) == this->m_phaseId
                        && it2->second.first == it->second[(m_handleIterator[it->first]+1)%2]
                        )
                {
                    it2 = addressProfiling::m_fifoTracker[this->m_sectionId][ownerPhase].erase(it2);
                }
                else
                {
                    it2++;
                }
            }
            if( tmpsize == addressProfiling::m_fifoTracker[this->m_sectionId][ownerPhase].size() )
            {
                std::cout << std::endl << "WARNING ! releaseHandles - We just tried to remove a R elt from m_fifoTracker but nothing changed !!!"
                        << std::endl << std::endl;
    //                    exit(0);
            }
        }
        for( std::map< eilck_obj*, eilck_ehdl*[2] >::iterator it = this->m_wAccesses.begin();
                it != this->m_wAccesses.end();
                ++it )
        {
            ParallelPhase* ownerPhase = addressProfiling::m_objTracker[it->first];
            PRINT_MSG_PARALLEL("m_fifoTracker releasing W handle " << it->second[(m_handleIterator[it->first]+1)%2] << " on obj " << it->first);
            PRINT_LOG_STREAM_TO_FILE();
            size_t tmpsize = addressProfiling::m_fifoTracker[this->m_sectionId][ownerPhase].size();
            std::list<std::pair<std::array<unsigned int,3>,std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>>::iterator it2 =
                    addressProfiling::m_fifoTracker[this->m_sectionId][ownerPhase].begin();
            while (it2 != addressProfiling::m_fifoTracker[this->m_sectionId][ownerPhase].end())
            {
                if ( std::get<0>(it2->first) == 0
                        && std::get<1>(it2->first) == this->m_taskId
                        && std::get<2>(it2->first) == this->m_phaseId
                        && it2->second.second == it->second[(m_handleIterator[it->first]+1)%2]
                        )
                {
                    it2 = addressProfiling::m_fifoTracker[this->m_sectionId][ownerPhase].erase(it2);
                }
                else
                {
                    it2++;
                }
            }
            if( tmpsize == addressProfiling::m_fifoTracker[this->m_sectionId][ownerPhase].size() )
            {
                std::cout << std::endl << "WARNING ! releaseHandles - We just tried to remove a W elt from m_fifoTracker but nothing changed !!!"
                        << "  - tmpsize = " << tmpsize
                        << std::endl << std::endl;
    //                    exit(0);
            }
        }
        lock1.unlock();
    #endif // PRINT_FIFOS
        for( std::map< eilck_obj*, eilck_ihdl*[2] >::const_iterator it = this->m_rAccesses.begin();
                it != this->m_rAccesses.end();
                ++it )
        {
            PRINT_MSG_PARALLEL("releasing R handle " << it->second[(m_handleIterator[it->first]+1)%2] << " on obj " << it->first);
            PRINT_LOG_STREAM_TO_FILE();
            eilck_ihdl_rel( it->second[(m_handleIterator[it->first]+1)%2] );
        }
        for( std::map< eilck_obj*, eilck_ehdl*[2] >::const_iterator it = this->m_wAccesses.begin();
                it != this->m_wAccesses.end();
                ++it )
        {
            PRINT_MSG_PARALLEL("releasing W handle " << it->second[(m_handleIterator[it->first]+1)%2] << " on obj " << it->first);
            PRINT_LOG_STREAM_TO_FILE();
            eilck_ehdl_rel( it->second[(m_handleIterator[it->first]+1)%2] );
        }
    }

    /// \brief acquires all Handles in m_wAccesses and m_rAccesses.
    /// Acquire blocks until the handle is at the head of the FIFO.
    void acquireHandles() const
    {
        for( std::map< eilck_obj*, eilck_ihdl*[2] >::const_iterator it = this->m_rAccesses.begin();
                it != this->m_rAccesses.end();
                ++it )
        {
            PRINT_MSG_PARALLEL("acquiring R handle " << it->second[m_handleIterator[it->first]] << " on obj " << it->first);
            PRINT_LOG_STREAM_TO_FILE();
            eilck_ihdl_acq(it->second[m_handleIterator[it->first]]);
        }
        for( std::map< eilck_obj*, eilck_ehdl*[2] >::const_iterator it = this->m_wAccesses.begin();
                it != this->m_wAccesses.end();
                ++it )
        {
            PRINT_MSG_PARALLEL("acquiring W handle " << it->second[m_handleIterator[it->first]] << " on obj " << it->first);
            PRINT_LOG_STREAM_TO_FILE();
            eilck_ehdl_acq(it->second[m_handleIterator[it->first]]);
        }
    }

    //isEmpty return true if this contains no memory accesses
    bool isEmpty() const
    {
        return m_handleIterator.empty();
    }
};

} // namespace verticalparallelization

#endif //HELPER_PARALLEL_PHASE_H

