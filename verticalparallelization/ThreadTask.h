///
/// \file   ThreadTask.h
/// \brief  Abstract class ThreadTask implementing the tasks used by the ThreadPool
/// \author Maxime Mogé
/// \date   2018
///
///
///
#ifndef HELPER_THREAD_TASK_H
#define HELPER_THREAD_TASK_H

#include <future>
#include "memory_var.h"
#include "memory_helper_functions.h"

namespace verticalparallelization
{

class ParallelTask;

/// \class ThreadTask
/// \brief  Abstract class implementing the tasks used by the ThreadPool
/// Templated on the return type of the Task -> not anymore, all Tasks return a void*.
/// Each task has a std::promise<int> attribute so we can wait for a task to complete
class ThreadTask
{

protected :
    /// identifier of the task
    unsigned int m_taskId;
    /// the promise of the result - its value is meaningless, it is only used to wait for the completion of the task
    std::promise<int> m_pr;

public:
    ThreadTask(unsigned int tid = 0 )
    {
        m_taskId = tid;
    }

    virtual ~ThreadTask()
    {
    }

    /// \brief runs the task
    virtual void run() = 0;

    /// gets a void ptr to the result returned by the execution of the Task
    /// the result needs to get casted to the right type after the call to getResult
    /// this way we can manage tasks returning different types of results
    virtual void* getResult() = 0;

    std::future<int> get_future()
    {
        return this->m_pr.get_future();
    }

    unsigned int getId()
    {
        return m_taskId;
    }

    /// At the end of a Task, we need to release the handles that are still acquired
    void releaseRemainingHandles()
    {
        if( !simuInfo::execType == ExecType::CLASSICAL )
        {
            ParallelTask*  curTask = getCurrentTask();
            if ( simuInfo::execType == ExecType::PARALLEL )
            {
                if( curTask->m_currentPhase )
                {
                    //if there was any memory access in this task
                    //we release the Handles on the current (and last) phase of current Task
                    //release the handles and require them immediately for next call to currentParallelSection
                    curTask->m_currentPhase->requireHandles();
                    curTask->m_currentPhase->releaseHandles();
                    curTask->m_currentPhaseAccessCounter = -1;
                }
            }
            //we need to set its m_accessCounter to 0 for next call to this Task
            else if ( simuInfo::execType == ExecType::LOGGING
                    && !currentSection::m_currentParallelSection->m_firstLoggingPhaseDone
                    && curTask->m_currentPhase )
            {
                //we are in first logging phase and there is at least one phase in current Task
                if ( curTask->m_currentPhaseAccessCounter < addressProfiling::m_memBlockSize-1
                        && curTask->m_currentPhaseAccessCounter != size_t(-1) )
                {
                    //there was at least one access in current phase, but the last access was not the m_memBlockSize-th
                    curTask->m_currentPhase->m_lastAccess = curTask->m_currentPhaseAccessCounter - 1;
                }
                curTask->m_currentPhaseAccessCounter = size_t(-1);
        //        curTask->m_currentPhaseAccessCounter = 0;
            }
            else if ( simuInfo::execType == ExecType::LOGGING
                    && !currentSection::m_currentParallelSection->m_secondLoggingPhaseDone
                    && curTask->m_currentPhase )
            {
                //we are in second logging phase and there is at least one phase in current Task
                curTask->m_currentPhaseAccessCounter = size_t(-1);
            }
            PRINT_MSG_PARALLEL(" from releaseRemainingHandles - TASK " << this->m_taskId << " DONE !!!");
            PRINT_LOG_STREAM_TO_FILE();
        }
    }

};

} // namespace verticalparallelization

#endif //HELPER_THREAD_TASK_H

