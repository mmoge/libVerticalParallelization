///
/// \file   memory_var.cpp
/// \brief
/// \author Maxime Mogé
/// \date   2018
///
///
///
#ifndef HELPER_MEMORY_VAR_CPP
#define HELPER_MEMORY_VAR_CPP

#include "memory_var.h"

namespace verticalparallelization
{

#ifdef PRINT_FIFOS
    std::map< ParallelSectionIdentifier, std::map< ParallelPhase*, std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>> > > addressProfiling::m_fifoTracker = std::map< ParallelSectionIdentifier, std::map< ParallelPhase*, std::list<std::pair< std::array<unsigned int,3>, std::pair<eilck_ihdl*[2],eilck_ehdl*[2]>>> > >();
    std::map< eilck_obj*, ParallelPhase* > addressProfiling::m_objTracker = std::map< eilck_obj*, ParallelPhase* >();
    std::mutex* addressProfiling::m_fifoTrackerMutex = new std::mutex();   //for thread safe use of m_fifoTracker
#endif // PRINT_FIFOS

    /// WARNING: m_memBlockSize value is defined by the used at the compilation on libverticalparallelization
    /// TODO: the lib could try to compute an appropriate value, or we could find a more user friendly way to set its value
//    unsigned int addressProfiling::m_memBlockSize = 320000;      //adi
    unsigned int addressProfiling::m_memBlockSize = 1024;    //hotspot3d
//    unsigned int addressProfiling::m_memBlockSize = 256;    //heat_3d
//    unsigned int addressProfiling::m_memBlockSize = 256;    //seidel_2d

    /// At the beginning of the execution, we run the parallel section sequentially
    ExecType simuInfo::execType = ExecType::CLASSICAL;

}   //namespace verticalparallelization

#endif  //HELPER_MEMORY_VAR_CPP
