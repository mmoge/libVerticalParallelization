///
/// \file   memory_parallelization.cpp
/// \brief
/// \author Maxime Mogé
/// \date   2018
///
///
///
#ifndef HELPER_MEMORY_PARALLELIZATION_CPP
#define HELPER_MEMORY_PARALLELIZATION_CPP

#include "memory_parallelization.h"

namespace verticalparallelization
{

    ThreadPool* threadPool::m_threadPool = new ThreadPool();
    unsigned int threadPool::m_taskCounter = 0;
    std::vector< std::future<int> > threadPool::m_futureVec = std::vector< std::future<int> >();
    std::vector< verticalparallelization::ThreadTask* > threadPool::m_taskVec = std::vector< verticalparallelization::ThreadTask* >();

    BaseParallelSection* getCurrentParallelSection()
    {
        return verticalparallelization::currentSection::m_currentParallelSection;
    }
}   //namespace verticalparallelization

#endif  //HELPER_MEMORY_PARALLELIZATION_CPP
