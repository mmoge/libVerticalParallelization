c compilation :
cc -std=gnu11 -Wall -O3 -march=native -g -ffast-math -fno-common -fdata-sections -ffunction-sections   -DTASKS=2 -I. -fPIC -IC -Wall -O3 -march=native -g -ffast-math -fno-common -fdata-sections -ffunction-sections  -fPIC -Wl,--gc-sections -lm -L/home/maxime/Work/Libraries/cmod_15_02_18/cmod/eilck/tests/rodinia/verticalparallelization/BUILD/ -lVerticalParallelization  hotspot3D.c   -o hotspot3D
exec :
./hotspot3D 512 2 100 data/hotspot3D/power_512x2 data/hotspot3D/temp_512x2 hotspot3D_512x2_100ite_reference.out




export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/maxime/Work/Libraries/libVerticalParallelization_30_07_18/verticalparallelization/BUILD/:/home/maxime/Work/Libraries/cmod/eilck/

g++ -std=c++14 -Wall -O3 -march=native -g -ffast-math -fno-common -fdata-sections -ffunction-sections   -DTASKS=2 -I. -fPIC -IC -Wall -O3 -march=native -g -ffast-math -fno-common -fdata-sections -ffunction-sections  -fPIC -Wl,--gc-sections -I/home/maxime/Work/Libraries/libVerticalParallelization_30_07_18/verticalparallelization/ -I/home/maxime/Work/Libraries/cmod/eilck -I/home/maxime/Work/Libraries/cmod/eilck/C  hotspot3D.cpp -lm -lpthread  -L/home/maxime/Work/Libraries/libVerticalParallelization_30_07_18/verticalparallelization/BUILD/ -lVerticalParallelization -L/home/maxime/Work/Libraries/cmod/eilck/ -leilck_noinit -o hotspot3Dcpp 
./hotspot3Dcpp 512 8 1000 data/hotspot3D/power_512x8 data/hotspot3D/temp_512x8 hotspot3D_512x8_1000_ite.out
diff hotspot3D_512x8_1000ite.out hotspot3Dcpp_512x8_1000_ite_reference.out

g++ -std=c++14 -Wall -O3 -march=native -g -ffast-math -fno-common -fdata-sections -ffunction-sections   -DTASKS=2 -I. -fPIC -IC -Wall -O3 -march=native -g -ffast-math -fno-common -fdata-sections -ffunction-sections  -fPIC -Wl,--gc-sections -I/home/maxime/Work/Libraries/libVerticalParallelization_30_07_18/verticalparallelization/ -I/home/maxime/Work/Libraries/cmod/eilck -I/home/maxime/Work/Libraries/cmod/eilck/C  hotspot3D_para.cpp -lm -lpthread  -L/home/maxime/Work/Libraries/libVerticalParallelization_30_07_18/verticalparallelization/BUILD/ -lVerticalParallelization -L/home/maxime/Work/Libraries/cmod/eilck/ -leilck_noinit -o hotspot3Dcpp_para 
./hotspot3Dcpp_para 512 8 1000 data/hotspot3D/power_512x8 data/hotspot3D/temp_512x8 hotspot3D_512x8_1000_ite.out
diff hotspot3D_512x8_1000ite.out hotspot3Dcpp_512x8_1000_ite_reference.out

g++ -std=c++14 -Wall -O3 -march=native -g -ffast-math -fno-common -fdata-sections -ffunction-sections   -DTASKS=2 -I. -fPIC -IC -Wall -O3 -march=native -g -ffast-math -fno-common -fdata-sections -ffunction-sections  -fPIC -Wl,--gc-sections -I/home/maxime/Work/Libraries/libVerticalParallelization_30_07_18/verticalparallelization/ -I/home/maxime/Work/Libraries/cmod/eilck -I/home/maxime/Work/Libraries/cmod/eilck/C  hotspot3D_para_unrolled.cpp -lm -lpthread  -L/home/maxime/Work/Libraries/libVerticalParallelization_30_07_18/verticalparallelization/BUILD/ -lVerticalParallelization -L/home/maxime/Work/Libraries/cmod/eilck/ -leilck_noinit -o hotspot3Dcpp_para_unrolled 
./hotspot3Dcpp_para_unrolled 512 8 1000 data/hotspot3D/power_512x8 data/hotspot3D/temp_512x8 hotspot3D_512x8_1000_ite.out
diff hotspot3D_512x8_1000ite.out hotspot3Dcpp_512x8_1000_ite_reference.out

g++ -std=c++14 -Wall -O3 -march=native -g -ffast-math -fno-common -fdata-sections -ffunction-sections   -DTASKS=2 -I. -fPIC -IC -Wall -O3 -march=native -g -ffast-math -fno-common -fdata-sections -ffunction-sections  -fPIC -Wl,--gc-sections -I/home/maxime/Work/Libraries/libVerticalParallelization_30_07_18/verticalparallelization/ -I/home/maxime/Work/Libraries/cmod/eilck -I/home/maxime/Work/Libraries/cmod/eilck/C  hotspot3D_para_unrolled4.cpp -lm -lpthread  -L/home/maxime/Work/Libraries/libVerticalParallelization_30_07_18/verticalparallelization/BUILD/ -lVerticalParallelization -L/home/maxime/Work/Libraries/cmod/eilck/ -leilck_noinit -o hotspot3Dcpp_para_unrolled4 
./hotspot3Dcpp_para_unrolled4 512 8 1000 data/hotspot3D/power_512x8 data/hotspot3D/temp_512x8 hotspot3D_512x8_1000_ite.out
diff hotspot3D_512x8_1000ite.out hotspot3Dcpp_512x8_1000_ite_reference.out

g++ -std=c++14 -Wall -O3 -march=native -g -ffast-math -fno-common -fdata-sections -ffunction-sections   -DTASKS=2 -I. -fPIC -IC -Wall -O3 -march=native -g -ffast-math -fno-common -fdata-sections -ffunction-sections  -fPIC -Wl,--gc-sections -I/home/maxime/Work/Libraries/libVerticalParallelization_30_07_18/verticalparallelization/ -I/home/maxime/Work/Libraries/cmod/eilck -I/home/maxime/Work/Libraries/cmod/eilck/C  hotspot3D_para_unrolled10.cpp -lm -lpthread  -L/home/maxime/Work/Libraries/libVerticalParallelization_30_07_18/verticalparallelization/BUILD/ -lVerticalParallelization -L/home/maxime/Work/Libraries/cmod/eilck/ -leilck_noinit -o hotspot3Dcpp_para_unrolled10 
./hotspot3Dcpp_para_unrolled10 512 8 1000 data/hotspot3D/power_512x8 data/hotspot3D/temp_512x8 hotspot3D_512x8_1000_ite.out
diff hotspot3D_512x8_1000ite.out hotspot3Dcpp_512x8_1000_ite_reference.out





icps-gc-6 : 
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/nfs/moge/verticalparallelization/BUILD/:/home/nfs/moge/cmod/eilck/:/home/nfs/moge/cmod/C
g++ -std=c++14 -Wall -O3 -march=native -g -ffast-math -fno-common -fdata-sections -ffunction-sections   -DTASKS=2 -I. -fPIC -IC -Wall -O3 -march=native -g -ffast-math -fno-common -fdata-sections -ffunction-sections  -fPIC -Wl,--gc-sections -fopenmp -I/home/nfs/moge/verticalparallelization/ -I/home/nfs/moge/cmod/eilck/ -I/home/nfs/moge/cmod/eilck/C -c hotspot3D_20_02_18_unrolled.cpp
g++ hotspot3D_20_02_18_unrolled.o -lm -lpthread  -L/home/nfs/moge/verticalparallelization/BUILD/ -lVerticalParallelization -L/home/nfs/moge/cmod/eilck/ -leilck_noinit -L/home/nfs/moge/cmod/C/ -lC -lgomp -o hotspot3Dcpp_20_02_18_unrolled 
./hotspot3Dcpp_20_02_18_unrolled 512 8 1000 data/hotspot3D/power_512x8 data/hotspot3D/temp_512x8 hotspot3D_512x8_1000ite_testunrolled.out

sudo LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/maxime/Work/Libraries/cmod_15_02_18/cmod/eilck/tests/rodinia/verticalparallelization/BUILD/:/home/maxime/Work/Libraries/cmod/eilck/ perf record  ./hotspot3Dcpp_reference_wrapper 512 2 100 data/hotspot3D/power_512x2 data/hotspot3D/temp_512x2 hotspot3Dcpp_512x2_100ite_test2.out
