#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
#include "wtime_inc.h"
#include "bfs_inc.h"

Node* read_nodes(FILE* fp, size_t no_of_nodes) {
  Node* h_graph_nodes = malloc(sizeof(Node[no_of_nodes]));
  // initalize the nodes
  for(size_t i = 0; i < no_of_nodes; i++) {
    fscanf(fp, "%u %u",
           &h_graph_nodes[i].starting,
           &h_graph_nodes[i].no_of_edges);
  }
  return h_graph_nodes;
}


int* read_edges(FILE* fp, size_t edge_list_size) {
  int* h_graph_edges = malloc(sizeof(int[edge_list_size]));
  for(size_t i=0; i < edge_list_size ; i++) {
    fscanf(fp,"%u %*d", &h_graph_edges[i]);
  }
  return h_graph_edges;
}

void Usage(int argc, char**argv){

  fprintf(stderr,"Usage: %s <input_file>\n", argv[0]);

}

////////////////////////////////////////////////////////////////////////////////
// Main Program
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//Apply BFS on a Graph
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv) {
  size_t no_of_nodes = 0;
  size_t edge_list_size = 0;

  if(argc!=2){
    Usage(argc, argv);
    exit(0);
  }

  char const* input_f = argv[1];

  printf("Reading File\n");
  //Read in Graph from a file
  FILE* fp = fopen(input_f,"r");
  if(!fp) {
    printf("Error: opening graph file\n");
    return EXIT_FAILURE;
  }

  int source = 0;

  fscanf(fp,"%zu", &no_of_nodes);
  if(no_of_nodes > INT_MAX) {
    printf("Error: %zu nodes are too much\n", no_of_nodes);
    return EXIT_FAILURE;
  }


  Node const*const restrict h_graph_nodes = read_nodes(fp, no_of_nodes);

  // allocate host memory
  unsigned*const restrict h_graph_dist      = memset(malloc(sizeof(unsigned[no_of_nodes])), 0xFF, sizeof(unsigned[no_of_nodes]));

  //read the source node from the file
  fscanf(fp,"%d", &source);
  // source=0; //tesing code line

  //set the source node as true in the mask
  h_graph_dist[source]    = 0;

  fscanf(fp,"%zu", &edge_list_size);
  if(edge_list_size > INT_MAX) {
    printf("Error: %zu edges are too much\n", edge_list_size);
    return EXIT_FAILURE;
  }

  int const*const restrict h_graph_edges = read_edges(fp, edge_list_size);

  if(fp) fclose(fp);

  printf("Start traversing the tree for graph with %zu nodes and %zu edges\n",
         no_of_nodes, edge_list_size);

  double start_time = wtime();
  size_t* marked = calloc(sizeof *marked, no_of_nodes);
  marked[0] = 1;

  for (size_t k = 0; marked[k]; ++k) {
    for(size_t tid = 0; tid < no_of_nodes; tid++) {
      if (h_graph_dist[tid] == k) {
        for(size_t i = h_graph_nodes[tid].starting;
            i < (h_graph_nodes[tid].starting + h_graph_nodes[tid].no_of_edges);
            i++) {
          size_t id = h_graph_edges[i];
          if(h_graph_dist[id] == -1) {
            h_graph_dist[id] = k+1;
            ++marked[k+1];
          }
        }
      }
    }
  }

  double end_time = wtime();
  printf("Compute time: %lf\n", (end_time - start_time));

  //Store the result into a file
  FILE *fpo = fopen("result.txt", "w");
  for(size_t i=0;i<no_of_nodes;i++)
    fprintf(fpo, "%zu) cost:%u\n", i, h_graph_dist[i]);
  fclose(fpo);
  printf("Result stored in result.txt\n");

  // cleanup
  free((void*)h_graph_nodes);
  free((void*)h_graph_edges);
  free(h_graph_dist);
  free(marked);

  return EXIT_SUCCESS;
}

