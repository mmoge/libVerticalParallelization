#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <complex.h>
#include <errno.h>
#include "wtime_inc.h"
#include "nn_inc.h"

/**
 ** The nearest neighbor benchmark as found in rodinia.
 **
 ** This is a pure C version that has been cleaned up from the
 ** original:
 **
 ** - use double consistently everywhere
 ** - use complex for a simple handling of 2D points
 ** - use unsigned types for indices
 ** - use a proper type for a db line
 ** - reorganize IO such that it correctly tests for eof
 ** - reorganize file opening such that db from the same directory as the
 **   list file are used
 **/

FILE* open_db(char* dir, char* slash, char* dbname) {
  char* nl = strrchr(dbname, '\n');
  if (nl) nl[0] = 0;
  if (slash && !strchr(dbname, '/')) {
    strcpy(slash, dbname);
    dbname = dir;
  }
  errno = 0;
  FILE* fp = fopen(dbname, "r");
  perror(dbname);
  if(!fp) error_exit(dbname);
  return fp;
}

/**
* This program finds the k-nearest neighbors
* Usage:	./nn <filelist> <num> <target latitude> <target longitude>
*			filelist: File with the filenames to the records
*			num: Number of nearest neighbors to find
*			target lat: Latitude coordinate for distance calculations
*			target long: Longitude coordinate for distance calculations
* The filelist and data are generated by hurricane_gen.c
* REC_WINDOW has been arbitrarily assigned; A larger value would allow more work for the threads
*/
int main(int argc, char* argv[]) {
  int    rec_count=0;
  size_t total = 0;

  if(argc < 5)
    error_exit("Invalid set of arguments\n");

  FILE* flist = fopen(argv[1], "r");
  if(!flist) error_exit(argv[1]);

  char dbdir[strlen(argv[1])+64];
  strcpy(dbdir, argv[1]);
  char* slash = strrchr(dbdir, '/');
  if (slash) {
    ++slash;
  }

  unsigned const k    = atoi(argv[2]);
  neighbor* neighbors = malloc(sizeof(neighbor[k]));

  if(!neighbors) error_exit("no room for neighbors");

  for(unsigned j = 0; j < k; j++ ) { //Initialize list of nearest neighbors to very large dist
    neighbors[j].dist = HUGE_VAL;
  }
  unsigned max_idx    = 0;

  complex double const target = CMPLX(convert(argv[3]), convert(argv[4]));

  FILE* fp = 0;

  static double z[REC_WINDOW];
  static dbline sandbox[REC_WINDOW];

  double time0 = wtime();
  for (bool done = false; !done;) {
    // Read in REC_WINDOW number of records
    for (rec_count = 0; !done && rec_count < REC_WINDOW;) {
      size_t recs = 128;
      if (rec_count+recs > REC_WINDOW) recs = REC_WINDOW-rec_count;
      while (recs && !done) {
        if(!fp || feof(fp)) {
          if (fp) fclose(fp);
          if(feof(flist)) done = true;
          else {
            char   dbname[64];
            if(!fgets(dbname, sizeof dbname, flist))
              error_exit("error reading dbname");
            if (dbname[0] && dbname[0] != '\n')
              fp = open_db(dbdir, slash, dbname);
            else done = true;
          }
        }
        if (fp) {
          int got = fread(sandbox[rec_count], sizeof sandbox[rec_count], recs, fp);
          for (int j = 0; j < got; ++j) {
            sandbox[rec_count+j][sizeof(dbline)-1] = 0;
          }
          total += got;
          rec_count += got;
          recs -= got;
          if (recs) {
            if (!feof(fp))
              error_exit("fread error");
            else {
              fclose(fp);
              fp = 0;
            }
          }
        }
      }
    }

    for (size_t i = 0; i < rec_count; i++) {
      char const* rec_iter = sandbox[i] + LATITUDE_POS - 1;
      complex double tmp = CMPLX(convert(rec_iter), convert(rec_iter+5));
      z[i] = cabs(tmp - target);
    }

    for(size_t i = 0; i < rec_count; i++) {
      // compare each record with max value to find the nearest neighbor
      if(z[i] < neighbors[max_idx].dist) {
        memcpy(neighbors[max_idx].entry, sandbox[i], sizeof(dbline));
        neighbors[max_idx].dist = z[i];
        // find a neighbor with greatest dist and take this spot later
        max_idx = 0;
        double max_dist = neighbors[0].dist;
        for(unsigned j = 1; j < k; j++) {
          if(neighbors[j].dist > max_dist) {
            max_dist = neighbors[j].dist;
            max_idx = j;
          }
        }
      }
    }
  } // End while loop
  double time1 = wtime();

  fprintf(stderr, "The %d nearest neighbors are:\n", k);
  for(unsigned j = 0; j < k; j++) {
    if(neighbors[j].dist != HUGE_VAL)
      fprintf(stderr, "%s --> %f\n", neighbors[j].entry, neighbors[j].dist);
  }

  fclose(flist);

  printf("total time : %15.12f s, %g s per record\n",
         time1 - time0,
         (time1 - time0)/total);
  free(neighbors);
  return 0;
}

