
#include <ctime>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include "wtime_inc.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <memory_var.h>
#include <memory_parallelization.h>
#include <DataWrapper.h>
#include <sys/time.h>

/*
 * Improvement of rodinia's hotspot3D code:
 *
 * - beautify the C
 * - use 3D matrices
 * - use size_t
 * - duplicate the inner computation to avoid pointer switching
 */


#define STR_SIZE (256)
#define MAX_PD  (3.0e6)
/* required precision in degrees    */
#define PRECISION   0.001
#define SPEC_HEAT_SI 1.75e6
#define K_SI 100
/* capacitance fitting factor   */
#define FACTOR_CHIP 0.5


struct timeval m_loggingTime = {0,0};
struct timeval m_totalComputeTime = {0,0};
static bool loggingtime = false;

bool parallelExecAllowed = false;


/* chip parameters  */
float t_chip = 0.0005;
float chip_height = 0.016; float chip_width = 0.016;
/* ambient temperature, assuming no package at all  */
float amb_temp = 80.0;

[[noreturn]]
void fatal(char *s) {
  fprintf(stderr, "Error: %s\n", s);
  exit(EXIT_FAILURE);
}

void readinput(size_t grid_rows, size_t grid_cols, size_t layers,
               std::vector< verticalparallelization::DataVectorWrapper<std::vector< std::vector< float >> > >& vect,
               char* file) {
  std::ifstream myfile;
  myfile.open(file);
  if (!myfile) std::cerr << "The file was not opened" << std::endl;

  for (size_t i=0; i <= grid_rows-1; i++)
    for (size_t j=0; j <= grid_cols-1; j++)
      for (size_t k=0; k <= layers-1; k++) {
        std::string str;
        if (getline(myfile,str).fail()) std::cerr << "Error reading file" << std::endl;
        if (myfile.eof()) std::cerr << "not enough lines in file" << std::endl;
        vect[k][i][j] = std::stof(str);
      }
  myfile.close();
}


void writeoutput(size_t grid_rows, size_t grid_cols, size_t layers,
                 std::vector< verticalparallelization::DataVectorWrapper<std::vector< std::vector< float >> > >& vect,
                 char* file) {
  std::ofstream myfile;
  myfile.open(file);
  if (!myfile) std::cerr << "The file was not opened" << std::endl;
  
  for (size_t i = 0, index = 0; i < grid_rows; i++)
    for (size_t j = 0; j < grid_cols; j++)
      for (size_t k = 0; k < layers; k++, index++) {
        myfile << index << "\t" << vect[k][i][j] << std::endl;
      }
  myfile.close();
}

/*
 * @brief the inner part of the hotspot3D computation.
 *
 * This allows us to repeat the same computation with inverted
 * arguments instead of switching the pointers.
 */
void computeInner(
        size_t nx, size_t ny, size_t nz,
        const std::vector< verticalparallelization::DataVectorWrapper<std::vector< std::vector< float >> > >& pIn,
        std::vector< verticalparallelization::DataVectorWrapper<std::vector< std::vector< float >> > >& tOut,
        const std::vector< verticalparallelization::DataVectorWrapper<std::vector< std::vector< float >> > >& tIn,
        float Cap,
        float dt,
      float cw,
      float ce,
      float cs,
      float cn,
      float cb,
      float ct,
      float cc
      )
{
    for(size_t z = 0; z < nz; z++) 
    {
      size_t z0 = (z == 0) ? 0      : z - 1;        
      size_t z1 = (z == nz - 1) ? z : z + 1;     
      for(size_t y = 0; y < ny; y++) {              
        size_t y0 = (y == 0) ? 0      : y - 1;      
        size_t y1 = (y == ny - 1) ? y : y + 1;      
        std::vector<  float >& toutzy = tOut[z][y];
        const std::vector< float >& tinzy = tIn[z][y];
        const std::vector< float >& tinzy0 = tIn[z][y0];
        const std::vector< float >& tinzy1 = tIn[z][y1];
        const std::vector< float >& tinz0y = tIn[z0][y];
        const std::vector< float >& tinz1y = tIn[z1][y];  
        const std::vector< float >& pinzy = pIn[z][y]; 
        for(size_t x = 0; x < nx; x++) {            
          size_t x0 = (x == 0) ? 0      : x - 1;    
          size_t x1 = (x == nx - 1) ? x : x + 1;    
          toutzy[x] = tinzy[x]*cc           
            + tinzy0[x]*cn + tinzy1[x]*cs   
            + tinzy[x1]*ce + tinzy[x0]*cw   
            + tinz1y[x]*ct + tinz0y[x]*cb   
            + (dt/Cap) * pinzy[x]               
            + ct*amb_temp;                          
        }                                           
      }
    }
}

void computeTempCPU(
        size_t nx, size_t ny, size_t nz,
        const std::vector< verticalparallelization::DataVectorWrapper<std::vector< std::vector< float >> > >& pIn,
        std::vector< verticalparallelization::DataVectorWrapper<std::vector< std::vector< float >> > >& tIn,
        std::vector< verticalparallelization::DataVectorWrapper<std::vector< std::vector< float >> > >& tOut,
        float Cap,
        float Rx, float Ry, float Rz,
        float dt, size_t numiter) {
  float stepDivCap = dt / Cap;
  float cw = stepDivCap/ Rx;
  float ce = cw;
  float cs = stepDivCap/ Ry;
  float cn = cs;
  float cb = stepDivCap/ Rz;
  float ct = cb;
  float cc = 1.0 - (2.0*ce + 2.0*cn + 3.0*ct);
  
  for (size_t i = 0; i < numiter/4; ++i) {
      std::vector< std::function<void()>* > computefunctions;
      std::tuple< size_t, size_t, size_t, std::uintptr_t > psid = std::tuple< size_t, size_t, size_t, std::uintptr_t >( nx,ny,nz, std::uintptr_t(&computeTempCPU) );
      if( parallelExecAllowed )
      {
          verticalparallelization::startParallelization< std::tuple< size_t, size_t, size_t, std::uintptr_t > >( psid );
      }
      verticalparallelization::beginParallelSection< void, std::tuple< size_t, size_t, size_t, std::uintptr_t > >( psid );

      unsigned int taskCounter = 0;
      struct  timeval c_start;
      struct  timeval c_end;
      gettimeofday(&c_start,NULL);
      //
      if( parallelExecAllowed )
      {
          computefunctions.push_back( new std::function<void()>( std::bind(computeInner, nx,ny,nz, std::ref(pIn),std::ref(tOut), std::ref(tIn),
                  Cap,dt,cw,ce,cs,cn,cb,ct,cc ) ) );
          verticalparallelization::runTaskVoid( *(computefunctions.back()) );
          taskCounter++;
          computefunctions.push_back( new std::function<void()>( std::bind(computeInner, nx,ny,nz, std::ref(pIn), std::ref(tIn),std::ref(tOut),
                  Cap,dt,cw,ce,cs,cn,cb,ct,cc ) ) );
          verticalparallelization::runTaskVoid( *(computefunctions.back()) );
          taskCounter++;
          //
          computefunctions.push_back( new std::function<void()>( std::bind(computeInner, nx,ny,nz, std::ref(pIn),std::ref(tOut), std::ref(tIn),
                  Cap,dt,cw,ce,cs,cn,cb,ct,cc ) ) );
          verticalparallelization::runTaskVoid( *(computefunctions.back()) );
          taskCounter++;
          computefunctions.push_back( new std::function<void()>( std::bind(computeInner, nx,ny,nz, std::ref(pIn), std::ref(tIn),std::ref(tOut),
                  Cap,dt,cw,ce,cs,cn,cb,ct,cc ) ) );
          verticalparallelization::runTaskVoid( *(computefunctions.back()) );
          taskCounter++;
      }
      else
      {
          computeInner(nx,ny,nz, pIn,tOut, tIn, Cap,dt,cw,ce,cs,cn,cb,ct,cc );
          taskCounter++;
          computeInner(nx,ny,nz, pIn, tIn,tOut, Cap,dt,cw,ce,cs,cn,cb,ct,cc );
          taskCounter++;
          computeInner(nx,ny,nz, pIn,tOut, tIn, Cap,dt,cw,ce,cs,cn,cb,ct,cc );
          taskCounter++;
          computeInner(nx,ny,nz, pIn, tIn,tOut, Cap,dt,cw,ce,cs,cn,cb,ct,cc );
          taskCounter++;
      }
      verticalparallelization::endParallelSectionVoid< std::tuple< size_t, size_t, size_t, std::uintptr_t > >( psid );
      //
      for (std::vector< std::function<void()>* >::iterator it = computefunctions.begin()
              ; it != computefunctions.end()
              ; ++it)
      {
          delete *it;

      }
      //
      gettimeofday(&c_end,NULL);
      timersub( &c_end, &c_start, &c_end );
      if( loggingtime )
      {
          timeradd( &m_loggingTime, &c_end, &m_loggingTime );
      }
      else
      {
          timeradd( &m_totalComputeTime, &c_end, &m_totalComputeTime );
      }
  }
}

float accuracy(size_t len, float *arr1, float *arr2) {
  float err = 0.0;
  size_t i;
  for(i = 0; i < len; i++) {
    err += (arr1[i]-arr2[i]) * (arr1[i]-arr2[i]);
  }

  return sqrtf(err/len);
}

void usage(int argc, char **argv) {
  fprintf(stderr, "Usage: %s <rows/cols> <layers> <iterations> <powerFile> <tempFile> <outputFile>\n", argv[0]);
  fprintf(stderr, "\t<rows/cols>\t- number of rows/cols in the grid (positive integer)\n");
  fprintf(stderr, "\t<layers>\t- number of layers in the grid (positive integer)\n");

  fprintf(stderr, "\t<iteration>\t- number of iterations (positive, even)\n");
  fprintf(stderr, "\t<powerFile>\t- name of the file containing the initial power values of each cell\n");
  fprintf(stderr, "\t<tempFile>\t- name of the file containing the initial temperature values of each cell\n");
  fprintf(stderr, "\t<outputFile>\t- output file\n");
  exit(EXIT_FAILURE);
}



int main(int argc, char** argv) {
  if (argc != 7) {
    usage(argc,argv);
  }

  char *pfile, *tfile, *ofile;
  size_t iterations = atoi(argv[3]);
  if (iterations%2) ++iterations;

  pfile = argv[4];
  tfile = argv[5];
  ofile = argv[6];

  size_t numCols = atoi(argv[1]);
  size_t numRows = numCols;
  size_t layers = atoi(argv[2]);

  /* calculating parameters*/

  float dx = chip_height/numRows;
  float dy = chip_width/numCols;
  float dz = t_chip/layers;

  float Cap = FACTOR_CHIP * SPEC_HEAT_SI * t_chip * dx * dy;
  float Rx = dy / (2.0 * K_SI * t_chip * dx);
  float Ry = dx / (2.0 * K_SI * t_chip * dy);
  float Rz = dz / (K_SI * dx * dy);

  float max_slope = MAX_PD / (FACTOR_CHIP * t_chip * SPEC_HEAT_SI);
  float dt = PRECISION / max_slope;


  std::vector< verticalparallelization::DataVectorWrapper<std::vector< std::vector< float >> > > powerIn( layers, verticalparallelization::DataVectorWrapper<std::vector< std::vector< float >> >(std::vector< std::vector< float >>(numRows, std::vector< float >(numCols))) );
  std::vector< verticalparallelization::DataVectorWrapper<std::vector< std::vector< float >> > > tempIn( layers, verticalparallelization::DataVectorWrapper<std::vector< std::vector< float >> >(std::vector< std::vector< float >>(numRows, std::vector< float >(numCols))) );
  std::vector< verticalparallelization::DataVectorWrapper<std::vector< std::vector< float >> > > answer( layers, verticalparallelization::DataVectorWrapper<std::vector< std::vector< float >> >(std::vector< std::vector< float >>(numRows, std::vector< float >(numCols))) );

  readinput(numRows, numCols, layers, powerIn, pfile);
  readinput(numRows, numCols, layers, tempIn,  tfile);

  parallelExecAllowed = true;
  double start = wtime();
  computeTempCPU(numCols, numRows, layers, powerIn, tempIn, answer, Cap, Rx, Ry, Rz, dt, iterations);

  for ( std::map< std::tuple< size_t, size_t, size_t, std::uintptr_t >, verticalparallelization::ParallelSection< std::tuple< size_t, size_t, size_t, std::uintptr_t > >* >::iterator it
              = verticalparallelization::ParallelSection< std::tuple< size_t, size_t, size_t, std::uintptr_t > >::m_existingSections.begin()
          ; it != verticalparallelization::ParallelSection< std::tuple< size_t, size_t, size_t, std::uintptr_t > >::m_existingSections.end()
          ;++it )
  {
      verticalparallelization::stopParallelization< std::tuple< size_t, size_t, size_t, std::uintptr_t > >( it->first );
  }

  double stop = wtime();
  double time = stop - start;

  printf("Time: %.3f (s)\n",time);
  std::cout << "logging time = " << m_loggingTime.tv_sec << "."
                << (m_loggingTime.tv_usec%1000)*1000 << " s." << std::endl;
  std::cout << "total compute time = " << m_totalComputeTime.tv_sec << "."
                << (m_totalComputeTime.tv_usec%1000)*1000 << "s." << std::endl;
  writeoutput(numRows, numCols, layers, answer, ofile);
//  free(tempIn);
//  free(powerIn);
//  free(answer);
  return 0;
}
