#ifndef BFS_H
#define BFS_H

typedef struct Node Node;
//Structure to hold a node information
struct Node {
  unsigned starting;
  unsigned no_of_edges;
};

#endif
