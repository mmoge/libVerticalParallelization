/**
 * This version is stamped on May 10, 2016
 *
 * Contact:
 *   Louis-Noel Pouchet <pouchet.ohio-state.edu>
 *   Tomofumi Yuki <tomofumi.yuki.fr>
 *
 * Web address: http://polybench.sourceforge.net
 */
/* adi.c: this file is part of PolyBench/C */

#include <stdio.h>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iostream>

/* Include polybench common header. */
#include "polybench_cpp.h"

/* Include benchmark-specific header. */
#include "adi.h"

#include <vector>
#include <sys/time.h>

struct timeval m_loggingTime = {0,0};
struct timeval m_totalComputeTime = {0,0};
static bool loggingtime = false;


/* Array initialization. */
static
void init_array (int n,
		 std::vector<std::vector<DATA_TYPE>>& u)
{
  int i, j;

  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      {
	u[i][j] =  (DATA_TYPE)(i + n-j) / n;
      }
}


/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static
void print_array(int n,
		 std::vector<std::vector<DATA_TYPE>>& u)
{
  int i, j;

  POLYBENCH_DUMP_START;
  POLYBENCH_DUMP_BEGIN("u");
  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++) {
      if ((i * n + j) % 20 == 0) fprintf(POLYBENCH_DUMP_TARGET, "\n");
      fprintf (POLYBENCH_DUMP_TARGET, DATA_PRINTF_MODIFIER, u[i][j]);
    }
  POLYBENCH_DUMP_END("u");
  POLYBENCH_DUMP_FINISH;
}


void computeInner1(
        size_t n,
        DATA_TYPE a, DATA_TYPE b, DATA_TYPE c, DATA_TYPE d, DATA_TYPE f,
		const std::vector<std::vector<DATA_TYPE>>& u,
		std::vector<std::vector<DATA_TYPE>>& v,
		std::vector<std::vector<DATA_TYPE>>& p,
		std::vector<std::vector<DATA_TYPE>>& q
      )
{
    for (int i=1; i<PB_N-1; i++) {
      v[0][i] = SCALAR_VAL(1.0);
      p[i][0] = SCALAR_VAL(0.0);
      q[i][0] = v[0][i];
      for (int j=1; j<PB_N-1; j++) {
        p[i][j] = -c / (a*p[i][j-1]+b);
        q[i][j] = (-d*u[j][i-1]+(SCALAR_VAL(1.0)+SCALAR_VAL(2.0)*d)*u[j][i] - f*u[j][i+1]-a*q[i][j-1])/(a*p[i][j-1]+b);
      }
    }
}
void computeInner2(
        size_t n,
		std::vector<std::vector<DATA_TYPE>>& v,
		const std::vector<std::vector<DATA_TYPE>>& p,
		const std::vector<std::vector<DATA_TYPE>>& q
      )
{
    for (int i=1; i<PB_N-1; i++) {
      v[PB_N-1][i] = SCALAR_VAL(1.0);
      for (int j=PB_N-2; j>=1; j--) {
        v[j][i] = p[i][j] * v[j+1][i] + q[i][j];
      }
    }
}
void computeInner3(
        size_t n,
        DATA_TYPE a, DATA_TYPE c, DATA_TYPE d, DATA_TYPE e, DATA_TYPE f,
		std::vector<std::vector<DATA_TYPE>>& u,
		const std::vector<std::vector<DATA_TYPE>>& v,
		std::vector<std::vector<DATA_TYPE>>& p,
		std::vector<std::vector<DATA_TYPE>>& q
      )
{
    for (int i=1; i<PB_N-1; i++) {
      u[i][0] = SCALAR_VAL(1.0);
      p[i][0] = SCALAR_VAL(0.0);
      q[i][0] = u[i][0];
      for (int j=1; j<PB_N-1; j++) {
        p[i][j] = -f / (d*p[i][j-1]+e);
        q[i][j] = (-a*v[i-1][j]+(SCALAR_VAL(1.0)+SCALAR_VAL(2.0)*a)*v[i][j] - c*v[i+1][j]-d*q[i][j-1])/(d*p[i][j-1]+e);
      }
    }
}
void computeInner4(
        size_t n,
		std::vector<std::vector<DATA_TYPE>>& u,
		const std::vector<std::vector<DATA_TYPE>>& p,
		const std::vector<std::vector<DATA_TYPE>>& q
      )
{
    for (int i=1; i<PB_N-1; i++) {
      u[i][PB_N-1] = SCALAR_VAL(1.0);
      for (int j=PB_N-2; j>=1; j--) {
        u[i][j] = p[i][j] * u[i][j+1] + q[i][j];
      }
    }
}

/* Main computational kernel. The whole function will be timed,
   including the call and return. */
/* Based on a Fortran code fragment from Figure 5 of
 * "Automatic Data and Computation Decomposition on Distributed Memory Parallel Computers"
 * by Peizong Lee and Zvi Meir Kedem, TOPLAS, 2002
 */
static
void kernel_adi(int tsteps, int n,
		std::vector<std::vector<DATA_TYPE>>& u,
		std::vector<std::vector<DATA_TYPE>>& v,
		std::vector<std::vector<DATA_TYPE>>& p,
		std::vector<std::vector<DATA_TYPE>>& q)
{
  DATA_TYPE DX, DY, DT;
  DATA_TYPE B1, B2;
  DATA_TYPE mul1, mul2;
  DATA_TYPE a, b, c, d, e, f;


  DX = SCALAR_VAL(1.0)/(DATA_TYPE)PB_N;
  DY = SCALAR_VAL(1.0)/(DATA_TYPE)PB_N;
  DT = SCALAR_VAL(1.0)/(DATA_TYPE)PB_TSTEPS;
  B1 = SCALAR_VAL(2.0);
  B2 = SCALAR_VAL(1.0);
  mul1 = B1 * DT / (DX * DX);
  mul2 = B2 * DT / (DY * DY);

  a = -mul1 /  SCALAR_VAL(2.0);
  b = SCALAR_VAL(1.0)+mul1;
  c = a;
  d = -mul2 / SCALAR_VAL(2.0);
  e = SCALAR_VAL(1.0)+mul2;
  f = d;

 for (int t=1; t<=PB_TSTEPS; t++) {
    struct  timeval c_start;
    struct  timeval c_end;
    gettimeofday(&c_start,NULL);
    //Column Sweep
    computeInner1( n,
        a, b, c,d,f,
        u,v,p,q);

    computeInner2( n,v,p,q);
    //Row Sweep
    computeInner3( n,
        a, c,d,e,f,
        u,v,p,q);
    computeInner4(n,u,p,q);
    gettimeofday(&c_end,NULL);
    timersub( &c_end, &c_start, &c_end );
    if( loggingtime )
    {
        timeradd( &m_loggingTime, &c_end, &m_loggingTime );
    }
    else
    {
        timeradd( &m_totalComputeTime, &c_end, &m_totalComputeTime );
    }
  }
}


int main(int argc, char** argv)
{
  /* Retrieve problem size. */
  int n = N;
  int tsteps = TSTEPS;

  /* Variable declaration/allocation. */
  std::vector<std::vector<DATA_TYPE>> u( n, std::vector<DATA_TYPE>( n ) );
  std::vector<std::vector<DATA_TYPE>> v( n, std::vector<DATA_TYPE>( n ) );
  std::vector<std::vector<DATA_TYPE>> p( n, std::vector<DATA_TYPE>( n ) );
  std::vector<std::vector<DATA_TYPE>> q( n, std::vector<DATA_TYPE>( n ) );


  /* Initialize array(s). */

  /* Start timer. */
  polybench_start_instruments;

  /* Run kernel. */
  kernel_adi (tsteps, n, u, v, p,q);

  /* Stop and print timer. */
  polybench_stop_instruments;
  polybench_print_instruments;
  std::cout << "logging time = " << m_loggingTime.tv_sec << "."
                << (m_loggingTime.tv_usec%1000)*1000 << " s." << std::endl;
  std::cout << "total compute time = " << m_totalComputeTime.tv_sec << "."
                << (m_totalComputeTime.tv_usec%1000)*1000 << "s." << std::endl;

  /* Prevent dead-code elimination. All live-out data must be printed
     by the function call in argument. */
  polybench_prevent_dce(print_array(n, u));

  /* Be clean. */
//  POLYBENCH_FREE_ARRAY(u);
//  POLYBENCH_FREE_ARRAY(v);
//  POLYBENCH_FREE_ARRAY(p);
//  POLYBENCH_FREE_ARRAY(q);

  return 0;
}
