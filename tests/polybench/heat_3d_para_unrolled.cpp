/**
 * This version is stamped on May 10, 2016
 *
 * Contact:
 *   Louis-Noel Pouchet <pouchet.ohio-state.edu>
 *   Tomofumi Yuki <tomofumi.yuki.fr>
 *
 * Web address: http://polybench.sourceforge.net
 */
/* heat-3d.c: this file is part of PolyBench/C */

#include <stdio.h>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iostream>

/* Include polybench common header. */
#include "polybench_cpp.h"

/* Include benchmark-specific header. */
#include "heat_3d.h"

#include <vector>
//#include <sys/time.h>

#include <memory_var.h>
#include <memory_var.cpp>
#include <memory_parallelization.h>
//#include <memory_parallelization.cpp>
#include <thread_info.cpp>
#include <DataWrapper.h>
#include <sys/time.h>

static bool timerlaunched = false;
struct timeval m_loggingTime = {0,0};
struct timeval m_totalComputeTime = {0,0};
static bool loggingtime = false;

bool parallelExecAllowed = false;

/* Array initialization. */
static
void init_array (int n,
		 std::vector< verticalparallelization::DataVectorWrapper< std::vector< std::vector<DATA_TYPE> > > > & A,
		 std::vector< verticalparallelization::DataVectorWrapper< std::vector< std::vector<DATA_TYPE> > > > & B)
{
  int i, j, k;

  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      for (k = 0; k < n; k++)
        A[i][j][k] = B[i][j][k] = (DATA_TYPE) (i + j + (n-k))* 10 / (n);
}


/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static
void print_array(int n,
		 std::vector< verticalparallelization::DataVectorWrapper< std::vector< std::vector<DATA_TYPE> > > > & A)

{
  int i, j, k;

  POLYBENCH_DUMP_START;
  POLYBENCH_DUMP_BEGIN("A");
  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      for (k = 0; k < n; k++) {
         if ((i * n * n + j * n + k) % 20 == 0) fprintf(POLYBENCH_DUMP_TARGET, "\n");
         fprintf(POLYBENCH_DUMP_TARGET, DATA_PRINTF_MODIFIER, A[i][j][k]);
      }
  POLYBENCH_DUMP_END("A");
  POLYBENCH_DUMP_FINISH;
}

void computeInner(
        size_t n,
        std::vector< verticalparallelization::DataVectorWrapper< std::vector< std::vector< DATA_TYPE > > > >& A,
        std::vector< verticalparallelization::DataVectorWrapper< std::vector< std::vector< DATA_TYPE > > > >& B
      )
{
    for (int i = 1; i < n-1; i++) {
        for (int j = 1; j < n-1; j++) {
            std::vector<DATA_TYPE>& Bij = B[i][j];
            std::vector<DATA_TYPE>& Aij = A[i][j];
            std::vector<DATA_TYPE>& Aip1j = A[i+1][j];
            std::vector<DATA_TYPE>& Aim1j = A[i-1][j];
            std::vector<DATA_TYPE>& Aijp1 = A[i][j+1];
            std::vector<DATA_TYPE>& Aijm1 = A[i][j-1];
            for (int k = 1; k < PB_N-1; k++) {
                Bij[k] =   SCALAR_VAL(0.125) * (Aip1j[k] - SCALAR_VAL(2.0) * Aij[k] + Aim1j[k])
                             + SCALAR_VAL(0.125) * (Aijp1[k] - SCALAR_VAL(2.0) * Aij[k] + Aijm1[k])
                             + SCALAR_VAL(0.125) * (Aij[k+1] - SCALAR_VAL(2.0) * Aij[k] + Aij[k-1])
                             + Aij[k];
            }
        }
    }
}

/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static
void kernel_heat_3d(int tsteps,
		      int n,
		      std::vector< verticalparallelization::DataVectorWrapper< std::vector< std::vector<DATA_TYPE> > > > & A,
		      std::vector< verticalparallelization::DataVectorWrapper< std::vector< std::vector<DATA_TYPE> > > > & B)
{
    for (int t = 1; t <= TSTEPS/2; t++) {
    struct  timeval c_start;
    struct  timeval c_end;
    gettimeofday(&c_start,NULL);


    std::vector< std::function<void()>* > computefunctions;
    std::tuple< size_t > psid = std::tuple< size_t>( n );
    if( parallelExecAllowed )
    {
        verticalparallelization::startParallelization< std::tuple< size_t > >( psid );
    }
    verticalparallelization::beginParallelSection< void, std::tuple< size_t > >( psid );

    //
    if( parallelExecAllowed )
    {
        computefunctions.push_back( new std::function<void()>( std::bind(computeInner, n, std::ref(A),std::ref(B) ) ) );
        verticalparallelization::runTaskVoid( *(computefunctions.back()) );
        computefunctions.push_back( new std::function<void()>( std::bind(computeInner, n, std::ref(B),std::ref(A) ) ) );
        verticalparallelization::runTaskVoid( *(computefunctions.back()) );
        //
        computefunctions.push_back( new std::function<void()>( std::bind(computeInner, n, std::ref(A),std::ref(B) ) ) );
        verticalparallelization::runTaskVoid( *(computefunctions.back()) );
        computefunctions.push_back( new std::function<void()>( std::bind(computeInner, n, std::ref(B),std::ref(A) ) ) );
        verticalparallelization::runTaskVoid( *(computefunctions.back()) );
        //
    }
    else
    {
        computeInner( n,A,B );
        computeInner( n,B,A );
        //
        computeInner( n,A,B );
        computeInner( n,B,A );
        //
    }
    verticalparallelization::endParallelSectionVoid< std::tuple< size_t > >( psid );
    //
    for (std::vector< std::function<void()>* >::iterator it = computefunctions.begin()
            ; it != computefunctions.end()
            ; ++it)
    {
        delete *it;

    }
    //
    gettimeofday(&c_end,NULL);
    timersub( &c_end, &c_start, &c_end );
    if( loggingtime )
    {
        timeradd( &m_loggingTime, &c_end, &m_loggingTime );
    }
    else
    {
        timeradd( &m_totalComputeTime, &c_end, &m_totalComputeTime );
    }
    
    }
}


int main(int argc, char** argv)
{
  /* Retrieve problem size. */
  int n = N;
  int tsteps = TSTEPS;

  /* Variable declaration/allocation. */
//  std::vector< verticalparallelization::DataVectorWrapper< std::vector< std::vector<DATA_TYPE> > > >  A( n, std::vector<std::vector<DATA_TYPE>>( n, std::vector<DATA_TYPE>(n) ) );
//  std::vector< verticalparallelization::DataVectorWrapper< std::vector< std::vector<DATA_TYPE> > > >  B( n, std::vector<std::vector<DATA_TYPE>>( n, std::vector<DATA_TYPE>(n) ) );
  std::vector< verticalparallelization::DataVectorWrapper< std::vector< std::vector< DATA_TYPE > > > > A( n, verticalparallelization::DataVectorWrapper<std::vector< std::vector< DATA_TYPE >> >(std::vector< std::vector< DATA_TYPE >>(n, std::vector< DATA_TYPE >(n))) );
  std::vector< verticalparallelization::DataVectorWrapper< std::vector< std::vector< DATA_TYPE > > > > B( n, verticalparallelization::DataVectorWrapper<std::vector< std::vector< DATA_TYPE >> >(std::vector< std::vector< DATA_TYPE >>(n, std::vector< DATA_TYPE >(n))) );


  /* Initialize array(s). */
  init_array (n, A, B);

  /* Start timer. */
//    struct  timeval c_start;
//    struct  timeval c_end;
//    gettimeofday(&c_start,NULL);
  polybench_start_instruments;

  parallelExecAllowed = true;
  /* Run kernel. */
  kernel_heat_3d (tsteps, n, A, B);

  /* Stop and print timer. */
//  gettimeofday(&c_end,NULL);
//  timersub( &c_end, &c_start, &c_end );
//  std::cout << "total time = " << c_end.tv_sec << " s. + "
//                << c_end.tv_usec << " µs." << std::endl;
  polybench_stop_instruments;
  polybench_print_instruments;
  std::cout << "logging time = " << m_loggingTime.tv_sec << "."
                << (m_loggingTime.tv_usec%1000)*1000 << " s." << std::endl;
  std::cout << "total compute time = " << m_totalComputeTime.tv_sec << "."
                << (m_totalComputeTime.tv_usec%1000)*1000 << "s." << std::endl;

  for ( std::map< std::tuple< size_t >, verticalparallelization::ParallelSection< std::tuple< size_t > >* >::iterator it
              = verticalparallelization::ParallelSection< std::tuple< size_t > >::m_existingSections.begin()
          ; it != verticalparallelization::ParallelSection< std::tuple< size_t > >::m_existingSections.end()
          ;++it )
  {
      verticalparallelization::stopParallelization< std::tuple< size_t > >( it->first );
  }

  /* Prevent dead-code elimination. All live-out data must be printed
     by the function call in argument. */
  polybench_prevent_dce(print_array(n, A));

//for(int i=0; i<n; ++i)
//for(int j=0; j<n; ++j)
//for(int k=0; k<n; ++k)
//std::cout<<A[i][j][k] << std::endl;
  /* Be clean. */
//  POLYBENCH_FREE_ARRAY(A);

  return 0;
}
