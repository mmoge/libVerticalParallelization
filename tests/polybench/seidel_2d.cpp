/**
 * This version is stamped on May 10, 2016
 *
 * Contact:
 *   Louis-Noel Pouchet <pouchet.ohio-state.edu>
 *   Tomofumi Yuki <tomofumi.yuki.fr>
 *
 * Web address: http://polybench.sourceforge.net
 */
/* seidel-2d.c: this file is part of PolyBench/C */

#include <stdio.h>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <iostream>

/* Include polybench common header. */
#include "polybench_cpp.h"

/* Include benchmark-specific header. */
#include "seidel_2d.h"

#include <vector>
#include <sys/time.h>

struct timeval m_loggingTime = {0,0};
struct timeval m_totalComputeTime = {0,0};
static bool loggingtime = false;

/* Array initialization. */
static
void init_array (int n,
		 std::vector<std::vector<DATA_TYPE>>& A)
{
  int i, j;

  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++)
      A[i][j] = ((DATA_TYPE) i*(j+2) + 2) / n;
}


/* DCE code. Must scan the entire live-out data.
   Can be used also to check the correctness of the output. */
static
void print_array(int n,
		 std::vector<std::vector<DATA_TYPE>>& A)

{
  int i, j;

  POLYBENCH_DUMP_START;
  POLYBENCH_DUMP_BEGIN("A");
  for (i = 0; i < n; i++)
    for (j = 0; j < n; j++) {
      if ((i * n + j) % 20 == 0) fprintf(POLYBENCH_DUMP_TARGET, "\n");
      fprintf(POLYBENCH_DUMP_TARGET, DATA_PRINTF_MODIFIER, A[i][j]);
    }
  POLYBENCH_DUMP_END("A");
  POLYBENCH_DUMP_FINISH;
}


void computeInner(
        size_t nbegin,
        size_t nend,
        size_t n,
        std::vector< std::vector< DATA_TYPE > >& A
      )
{
    for (int i = nbegin; i<= nend; i++)
    {
      std::vector< DATA_TYPE >& Aim1 = A[i-1];
      std::vector< DATA_TYPE >& Ai = A[i];
      std::vector< DATA_TYPE >& Aip1 = A[i+1];
      for (int j = 1; j <= PB_N - 2; j++)
	    Ai[j] = (Aim1[j-1] + Aim1[j] + Aim1[j+1]
		   + Ai[j-1] + Ai[j] + Ai[j+1]
		   + Aip1[j-1] + Aip1[j] + Aip1[j+1])/SCALAR_VAL(9.0);
    }
}
/* Main computational kernel. The whole function will be timed,
   including the call and return. */
static
void kernel_seidel_2d(int tsteps,
		      int n,
		      std::vector<std::vector<DATA_TYPE>>& A)
{
std::cout << "nb iterations = " << PB_TSTEPS << std::endl;
  for (int t = 0; t <= PB_TSTEPS - 1; t++)
  {
    struct  timeval c_start;
    struct  timeval c_end;
    gettimeofday(&c_start,NULL);
    
    computeInner(1,PB_N - 2,n,A);
    gettimeofday(&c_end,NULL);
    timersub( &c_end, &c_start, &c_end );
    if( loggingtime )
    {
        timeradd( &m_loggingTime, &c_end, &m_loggingTime );
    }
    else
    {
        timeradd( &m_totalComputeTime, &c_end, &m_totalComputeTime );
    }
  }

}


int main(int argc, char** argv)
{
  /* Retrieve problem size. */
  int n = N;
  int tsteps = TSTEPS;

  /* Variable declaration/allocation. */
  std::vector<std::vector<DATA_TYPE>> A( n, std::vector<DATA_TYPE>( n ) );


  /* Initialize array(s). */
  init_array (n, A);

  /* Start timer. */
  polybench_start_instruments;

  /* Run kernel. */
  kernel_seidel_2d (tsteps, n, A);

  /* Stop and print timer. */
  polybench_stop_instruments;
  polybench_print_instruments;
  std::cout << "logging time = " << m_loggingTime.tv_sec << "."
                << (m_loggingTime.tv_usec%1000)*1000 << " s." << std::endl;
  std::cout << "total compute time = " << m_totalComputeTime.tv_sec << "."
                << (m_totalComputeTime.tv_usec%1000)*1000 << "s." << std::endl;

  /* Prevent dead-code elimination. All live-out data must be printed
     by the function call in argument. */
  polybench_prevent_dce(print_array(n, A));

  /* Be clean. */
//  POLYBENCH_FREE_ARRAY(A);

  return 0;
}
