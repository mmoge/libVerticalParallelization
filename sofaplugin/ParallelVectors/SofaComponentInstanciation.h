/******************************************************************************
*       SOFA, Simulation Open-Framework Architecture, development version     *
*                (c) 2006-2017 INRIA, USTL, UJF, CNRS, MGH                    *
*                                                                             *
* This program is free software; you can redistribute it and/or modify it     *
* under the terms of the GNU Lesser General Public License as published by    *
* the Free Software Foundation; either version 2.1 of the License, or (at     *
* your option) any later version.                                             *
*                                                                             *
* This program is distributed in the hope that it will be useful, but WITHOUT *
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       *
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License *
* for more details.                                                           *
*                                                                             *
* You should have received a copy of the GNU Lesser General Public License    *
* along with this program. If not, see <http://www.gnu.org/licenses/>.        *
*******************************************************************************
* Authors: The SOFA Team and external contributors (see Authors.txt)          *
*                                                                             *
* Contact information: contact@sofa-framework.org                             *
******************************************************************************/
#ifndef SOFA_COMPONENT_INSTANCIATION_H
#define SOFA_COMPONENT_INSTANCIATION_H

#include "ParallelTypes.h"
#include <sofa/core/ObjectFactory.h>

#include <SofaBaseMechanics/MechanicalObject.h>
#include <sofa/core/Mapping.h>
#include <SofaBaseMechanics/BarycentricMapping.h>

#include <SofaBaseTopology/TriangleSetGeometryAlgorithms.h>
#include <SofaBaseTopology/TetrahedronSetGeometryAlgorithms.h>
#include <SofaBaseTopology/QuadSetGeometryAlgorithms.h>
#include <SofaBaseTopology/EdgeSetGeometryAlgorithms.h>
#include <SofaBaseTopology/HexahedronSetGeometryAlgorithms.h>

//#include <sofa/core/behavior/ForceField.h>
#include "SofaBoundaryCondition/ConstantForceField.h"
#include "SofaBoundaryCondition/FixedConstraint.h"

#include "SofaSimpleFem/TetrahedronFEMForceField.h"
#include "SofaGeneralSimpleFem/TetrahedralCorotationalFEMForceField.h"
#include "SofaBaseMechanics/DiagonalMass.h"
#include "SofaMiscForceField/MeshMatrixMass.h"


namespace sofa
{

namespace component
{


} // namespace component

} // namespace sofa



#endif //SOFA_COMPONENT_INSTANCIATION_H
